<?php
    if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die(_e('You are not allowed to call this page directly.','evbox_bgpat')); }
?>   
    
		<form method="post" action="options.php" class="col ">			
			<?php settings_fields('evbox_bgpat_license'); ?>
	
			<table class="form-table license">
				<tbody>
					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e('License status'); ?>
						</th>
						<td class="field">
							<?php if($status != "valid") : ?>
							<span class="status red white-text darken-1">INACTIVE</span> - you are not receiving updates
							<?php else : ?>
							<span class="status green white-text darken-1">ACTIVE</span> - you are receiving updates
							<?php endif ?>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e('License Key'); ?>
						</th>
						<td class="field">
							<input id="evbox_bgpat_license_key" name="evbox_bgpat_license_key" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
							<label class="description" for="evbox_bgpat_license_key"><?php _e('Enter your license key'); ?></label>
						</td>
					</tr>
					<?php if( false !== $license ) { ?>
						<tr valign="top">
							<th scope="row" valign="top">
								<?php _e('Activate License'); ?>
							</th>
							<td>
								<?php if( $status !== false && $status == 'valid' ) { ?>
									<?php wp_nonce_field( 'evbox_bgpat_nonce', 'evbox_bgpat_nonce' ); ?>
									<input type="submit" class="button-secondary" name="evbox_bgpat_deactivate" value="<?php _e('Deactivate License'); ?>"/>
								<?php } else { ?>
									<?php wp_nonce_field( 'evbox_bgpat_nonce', 'evbox_bgpat_nonce' ); ?>
									<input type="submit" class="button-secondary" name="evbox_bgpat_activate" value="<?php _e('Activate License'); ?>"/>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php submit_button(); ?>
	
		</form>
