<?php

define( 'EVBOX_BGPAT_VERSION', "1.0");
define( 'EVBOX_BGPAT_STORE_URL', 'https://www.greentreelabs.net/' );
define( 'EVBOX_BGPAT', 'EverlightBox Background Patterns' );

if(! class_exists( 'EDD_SL_Plugin_Updater' )) {
	include( 'EDD_SL_Plugin_Updater.php' );
}


class EvBoxBgPatterns {

	public function __construct( ) {
		add_action('admin_init', array($this, 'plugin_updater'), 0);
		add_action('admin_init', array($this, 'deactivate_license'));
		add_action('admin_init', array($this, 'activate_license'));
		add_action('admin_init', array($this, 'register_option'));
		add_action('everlightbox_menu', array($this, 'admin_menu'));		
	}

	public function admin_menu() {
		$license = add_submenu_page('everlightbox_options', __('Background patterns License','evbox_bgpat'), __('Background patterns','evbox_bgpat'), 'edit_posts', 'evbox_bgpat-license', array($this, 'license'));
	}

	public function license() {			
		$license 	= get_option( 'evbox_bgpat_license_key' );
		$status 	= get_option( 'evbox_bgpat_license_status' );	
		
		include("license-page.php");
	}

	public function register_option() 
	{
		register_setting('evbox_bgpat_license', 'evbox_bgpat_license_key', array($this, 'sanitize_license'));
	}
	
	public function sanitize_license( $new ) 
	{
		$old = get_option( 'evbox_bgpat_license_key' );
		if( $old && $old != $new ) 
		{
			delete_option( 'evbox_bgpat_license_status' ); // new license has been entered, so must reactivate
		}
		return $new;
	}
	
	public function plugin_updater() 
	{
		$license_key = trim( get_option( 'evbox_bgpat_license_key' ) );
	
		// setup the updater
		$edd_updater = new EDD_SL_Plugin_Updater( EVBOX_BGPAT_STORE_URL, __FILE__, array(
				'version' 	=> EVBOX_BGPAT_VERSION,
				'license' 	=> $license_key,
				'item_name' => EVBOX_BGPAT,
				'author' 	=> 'GreenTreeLabs'
			)
		);			
	}
	
	public function activate_license() 
	{
		if( isset( $_POST['evbox_bgpat_activate'] ) ) 
		{
		 	if( ! check_admin_referer( 'evbox_bgpat_nonce', 'evbox_bgpat_nonce' ) )
				return; // get out if we didn't click the Activate button

			$license = trim( get_option( 'evbox_bgpat_license_key' ) );
			
			// data to send in our API request
			$api_params = array(
				'edd_action'=> 'activate_license',
				'license' 	=> $license,
				'item_name' => urlencode( EVBOX_BGPAT ), // the name of our product in EDD
				'url'       => home_url()
			);
	
			// Call the custom API.
			$response = wp_remote_post( EVBOX_BGPAT_STORE_URL , array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
	
			// make sure the response came back okay
			if ( is_wp_error( $response ) )
				return false;
	
			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
	
			// $license_data->license will be either "valid" or "invalid"
	
			update_option( 'evbox_bgpat_license_status', $license_data->license );
	
		}
	}
	
	public function deactivate_license() 
	{
		if( isset( $_POST['evbox_bgpat_deactivate'] ) ) 
		{		
		 	if( ! check_admin_referer( 'evbox_bgpat_nonce', 'evbox_bgpat_nonce' ) )
				return;
	
			// retrieve the license from the database
			$license = trim( get_option( 'evbox_bgpat_license_key' ) );
	
			// data to send in our API request
			$api_params = array(
				'edd_action'=> 'deactivate_license',
				'license' 	=> $license,
				'item_name' => urlencode( EVBOX_BGPAT ), // the name of our product in EDD
				'url'       => home_url()
			);
	
			// Call the custom API.
			$response = wp_remote_post( EVBOX_BGPAT_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
	
			// make sure the response came back okay
			if ( is_wp_error( $response ) ) 
				return false;
	
			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
	
			// $license_data->license will be either "deactivated" or "failed"
			if( $license_data->license == 'deactivated' )
				delete_option( 'evbox_bgpat_license_status' );
	
		}
	}
}

$evProBgPatterns = new EvBoxBgPatterns();