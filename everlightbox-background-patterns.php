<?php

/**
 * @link              http://everlightbox.io
 * @since             1.0.0
 * @package           Everlightbox Background Patterns
 *
 * @wordpress-plugin
 * Plugin Name:       EverlightBox Background Patterns
 * Plugin URI:        everlightbox-background-patterns
 * Description:       Background patterns for EverlightBox
 * Version:           1.0.0
 * Author:            GreenTreeLabs
 * Author URI:        http://www.greentreelabs.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       everlightbox-bp
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

include( dirname( __FILE__ ) . '/license/class-bgpat-license.php' );

define("EVERLIGHTBOX_BP_VERSION", "1.0.0");


class Everlightbox_Background_Patterns {

	protected $options = null;
	protected $patterns = null;

	public function __construct( ) {
		$this->patterns = array(
			'1'	 => 'Soft dots',
			'2'	 => 'Tartan',
			'3'	 => 'Thin grain',
			'4'	 => 'Oblique lines',
			'5'  => 'Circles',
			'6'  => 'Hearts',
			'7'  => 'Grid',
			'8'  => 'Squared dots',
			'9'  => 'Horizontal strips',
			'10' => 'Vertical strips',
			'11' => 'Dots',
			'12' => 'Stars',
			'13' => 'Diamonds',
			'14' => 'Hexagons',
			'15' => 'Labyrinth',
			'16' => 'Cubes'
		);

		add_filter( 'everlightbox_additional_fields', array($this, 'add_field') );
		add_action( 'wp_head', array($this, 'style') );
		$this->options = get_option('everlightbox_options');
	}

	public function style() { 

		if(isset($this->options['bg_pattern'])) {

		?>
		<style>
			#everlightbox-overlay {
				background-image: url('<?php echo plugin_dir_url( __FILE__ ) ?>img/<?php echo $this->options['bg_pattern'] ?>-min.png');
			}
		</style>
		<?php 
		}
	}

	public function add_field($options) {

		$options->add_field( array(
		    'name'    => 'Background pattern',
		    'desc'	  => '',
		    'id'      => 'bg_pattern',
		    'type'    => 'select',
		    'show_option_none' => true,
		    'options' => $this->patterns,
		    'row_classes' => 'el-tab-1'
		) );

		return $options;
	}

}

$everlightboxBackgroundPatterns = new Everlightbox_Background_Patterns();