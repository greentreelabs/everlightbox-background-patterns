=== EverlightBox Background Patterns ===
Contributors: GreenTreeLabs
Donate link: https://www.greentreelabs.net/
Tags: lightbox
Requires at least: 3.8.0
Tested up to: 4.7
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Background patterns for EverlightBox


== Changelog ==

= 1.0.0 =
* First release

== Upgrade Notice ==

= 1.0.0 =
* First release
